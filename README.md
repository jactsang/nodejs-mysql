# nodejs-mysql

The is a demo for building backend with node.js and MySQL

### Start

```
npm install && npm start
```

### Environment

1. PORT: 8080 (default)
2. MYSQL_PASS: user password for MySQL database
3. CRYPT_KEY: key to encrypt jwt secret
4. TOKEN_SECRET: encrypted jwt secret