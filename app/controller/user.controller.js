import Cryptr from 'cryptr';
import * as JWT from 'jsonwebtoken';
import { JWTCONFIG } from '../config/config.js';
import db from '../models/index.js';

const Users = db.Users;
const Op = db.Sequelize.Op;
const cryptr = new Cryptr(JWTCONFIG.CRPYTKEY);

const create = async (req, res) => {
  const object = {
    title: req.body.title,
    description: req.body.description || 'N/A',
    published: req.body.published ? req.body.published : false
  };
  await Users.create(object)
    .then(data => {
      console.log('@@@[controller/ user] create() - data: ', data);
      res.send(data);
    })
    .catch(err => {
      console.log('@@@[controller/ user] create() - err: ', err);
      res.status(500).send({
        message: err.message || 'Failed to create item.'
      });
    })
}

const findAll = async (req, res) => {
  try {
    const { email } = req.query;
    if (!email) {
      throw { message: 'Please provide email.', status: 400 };
    }
    const condition = email ? { title: { [Op.like]: `%${email}%` } } : null;
    await Users.findAll({ where: condition })
      .then(data => {
        console.log('@@@[controller/ user] findAll() - data: ', data);
        res.send(data);
      })
      .catch(err => {
        console.log('@@@[controller/ user] findAll() - err: ', err);
        res.status(500).send({
          message: err.message || 'Failed to get all items.'
        });
      })
  } catch (err) {
    res.status(err.status || 500).send({
      message: err.message || 'Internal Error'
    });
  }

}

const authenticate = async (req, res) => {
  try {
    const { email, password } = req.query;
    if (!email) {
      throw { message: 'Invalid email.', status: 400 };
    }
    if (!password) {
      throw { message: 'Invalid password.', status: 400 };
    }
    await Users.findOne({ where: { email, password } })
      .then(data => {
        const secret = cryptr.decrypt(JWTCONFIG.TOKEN_SECRET);
        const token = JWT.sign(data, secret, { expiresIn: expiryInSeconds })
        res.send({ token });
      })
      .catch(err => {
        res.status(500).send({
          message: 'Failed to authenticate login details.'
        });
      });
  } catch (e) {
    res.status(err.status || 500).send({
      message: err.message || 'Internal Error'
    });
  }
}

export {
  create,
  findAll,
  authenticate
}