import { DBCONFIG } from '../config/config.js';
import Sequelize from 'sequelize';
import { user } from './user.model.js';

const { DB, USER, PASSWORD, HOST, dialect, pool } = DBCONFIG;
const sequelize = new Sequelize(DB, USER, PASSWORD, {
  host: HOST,
  dialect,
  operatorsAliases: false,
  pool: {
    max: pool.max,
    min: pool.min,
    acquire: pool.acquire,
    idle: pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = user(sequelize, Sequelize);

export default db;