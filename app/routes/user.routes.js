import { create, findAll, authenticate } from '../controller/user.controller.js';
import express from 'express';

const ROUTES = (app) => {
  const router = express.Router();
  router.post('/create', create);
  router.get('/all', findAll);
  router.get('/authenticate', authenticate);
  app.use('/api/account', router);
}

export default ROUTES;