
import dotenv from 'dotenv';
dotenv.config();

const DBCONFIG = {
  HOST: 'localhost',
  USER: 'root',
  PASSWORD: process.env.MYSQL_PASS,
  DB: 'testdb',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};

const JWTCONFIG = {
  CRPYTKEY: process.env.CRYPT_KEY
};

export {
  DBCONFIG,
  JWTCONFIG
};